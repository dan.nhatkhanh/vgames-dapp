import Head from 'next/head'
import MyInventory from 'views/MyInventory'

const MyInventoryPage = () => {
  return (
    <>
      <Head>
        <title>My Inventory</title>
      </Head>
      <MyInventory />
    </>
  )
}

export default MyInventoryPage
