import Head from 'next/head'
import MinNFT from 'views/MintNFT'

const MintNFTPage = () => {
  return (
    <>
      <Head>
        <title>Mint NFT</title>
      </Head>
      <MinNFT />
    </>
  )
}

export default MintNFTPage
