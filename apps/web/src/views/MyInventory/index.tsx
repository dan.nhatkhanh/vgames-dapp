import { Loading } from '@pancakeswap/uikit'
import { useEffect, useState } from 'react'
import { FaListUl } from 'react-icons/fa'
import { IoMdGrid } from 'react-icons/io'
import styled from 'styled-components'
import { useAccount } from 'wagmi'
import { Web3 } from 'web3'
import contractAbi from './contractAbi.json'

const StyledPage = styled.div`
  min-height: calc(100vh - 64px);
  width: 100vw;
  position: relative;
  ${({ theme }) => theme.mediaQueries.sm} {
    padding-top: 24px;
    padding-bottom: 24px;
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    padding-top: 32px;
    padding-bottom: 32px;
  }
`

const StyleNFTImage = styled.div`
  background: url('/images/nft.avif') center center / contain no-repeat;
  width: 100%;
  aspect-ratio: 1/1;
  border-radius: 10px;
  transition: 0.5s all;

  &:hover {
    transform: scale(1.05);
  }
`

const StyleGrid = styled.div`
  display: grid;
  gap: 20px;
  grid-template-columns: auto;
  padding: 20px auto;
  height: 50vh;
  overflow-y: scroll;
  margin: auto 50px;

  ${({ theme }) => theme.mediaQueries.sm} {
    padding-top: 24px;
    padding-bottom: 24px;
    grid-template-columns: auto auto auto;
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    padding-top: 32px;
    padding-bottom: 32px;
    grid-template-columns: auto auto auto auto auto;
  }
`

const StyleCardBox = styled.div`
  border-radius: 10px;
  box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
`

const StyleNFTInfoBox = styled.div`
  padding: 5px 10px 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 10px;
  width: 100%;
`

const StyleTokenIdText = styled.p`
  font-size: 20px;
`

const StyleTitle = styled.h2`
  width: fit-content;
  margin: 20px auto;
  font-size: 50px;
`

const StylePopWrapper = styled.div`
  background: #00000090;
  padding: 20px;
  position: absolute;
  height: 100vh;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const StylePopUp = styled.div`
  background: #27262c;
  padding: 20px;
  position: absolute;
`

const StyleConnectWalletWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  align-items: center;
`

const StyleTableWrapper = styled.div`
  width: 90%;
  border: 1px solid #cccccc50;
  border-radius: 20px;
  padding: 20px;
  margin: 20px auto;
  overflow-y: auto;
  height: 50vh;
`

const StyleTable = styled.table`
  width: 100%;
  margin: 0px auto;
  border-collapse: collapse;
`

const StyleRowItemBox = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`

const StyleNFTImageInList = styled.div`
  background: url('/images/nft.avif') center center / contain no-repeat;
  width: 60px;
  aspect-ratio: 1/1;
  border-radius: 10px;
  transition: 0.5s all;

  &:hover {
    transform: scale(1.05);
  }
`

const StyleTableRow = styled.tr`
  transition: 0.3s all;
  &:hover {
    background: #cccccc80;
  }
`

const StyleTableFirstTh = styled.th`
  position: sticky;
  top: 0;
  border-bottom: 1px;
  background: gray;
  color: #fff;
  border-top-left-radius: 10px;
  padding: 15px 0;
`

const StyleTableLastTh = styled.th`
  position: sticky;
  top: 0;
  border-bottom: 1px;
  background: gray;
  color: #fff;
  border-top-right-radius: 10px;
  padding: 15px 0;
`

const StyleTableTh = styled.th`
  position: sticky;
  top: 0;
  border-bottom: 1px;
  background: gray;
  color: #fff;
  padding: 15px 0;
`

const StyleTableBody = styled.tbody`
  height: 50px;
  overflow: scroll;
`

const StyleDisplayTypeWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyleGridType = styled(IoMdGrid)`
  padding: 2px;
  margin-right: 5px;
  border: 1px solid transparent;
  border-radius: 8px;
  cursor: pointer;
  &:hover {
    border: 1px solid;
  }
`

const StyleListType = styled(FaListUl)`
  padding: 4px;
  margin-left: 5px;
  border: 1px solid transparent;
  border-radius: 8px;
  cursor: pointer;
  &:hover {
    border: 1px solid;
  }
`

interface ITokenData {
  transactionHash: string
  blockNumber: bigint
  from_address: string
  to_address: string
  token_id: string
}

function MyInventory() {
  const account = useAccount()
  const [tokenList, setTokenList] = useState<ITokenData[] | null>(null)
  const [isLoading, setIsLoading] = useState(false)
  const displayTypeFromLocalStorage = localStorage.getItem('DISPLAY-TYPE') as 'grid' | 'list'
  const [displayType, setDisplayType] = useState<'grid' | 'list'>(displayTypeFromLocalStorage)

  const listTokensOfOwner = async (address: string) => {
    setIsLoading(true)
    const web3 = new Web3('https://data-seed-prebsc-2-s2.bnbchain.org:8545')

    const currentBlockNumber = await web3.eth.getBlockNumber()
    const contract: any = new web3.eth.Contract(contractAbi, '0xF2e5482c230cb62b1363039E4128a8f2A7Ce8f5D')
    let firstBlockNumber = 38670434
    const count = Math.floor(Number(currentBlockNumber) / 100000)
    const data: any[] = []
    const temp = new Array(count).fill(1)
    for await (const iterator of temp) {
      const events = await contract.getPastEvents('Transfer', {
        filter: { to: address },
        fromBlock: firstBlockNumber,
        toBlock: firstBlockNumber + 100000,
      })
      // console.log(It has ${events.length} transactions)
      data.push(...events)
      firstBlockNumber += 100000
      // console.log(===============================================)
      if (firstBlockNumber > currentBlockNumber) {
        break
      }
    }
    const result: ITokenData[] = []
    for (let i = 0; i < data.length; i++) {
      const dto = {
        transactionHash: data[i]?.transactionHash,
        blockNumber: data[i]?.blockNumber,
        from_address: data[i]?.returnValues.from,
        to_address: data[i]?.returnValues.to,
        token_id: `#${Number(data[i]?.returnValues.tokenId)}`,
      }
      result.push(dto)
    }

    setTokenList(result)
    setIsLoading(false)
    return result
  }

  useEffect(() => {
    if (account.address) {
      listTokensOfOwner(account.address)
    }
  }, [account.address])

  const onRenderNftGrid = () => {
    return tokenList?.map((token, i) => {
      return (
        <StyleCardBox key={token.token_id}>
          <StyleNFTImage />
          <StyleNFTInfoBox>
            <StyleTokenIdText>Vgames {token.token_id}</StyleTokenIdText>
            <p style={{ padding: '4px', border: '1px solid #ccc', borderRadius: '8px' }}>#{167 + i}</p>
          </StyleNFTInfoBox>
          <p style={{ padding: '0px 10px 20px' }}> 0.1025 ETH</p>
        </StyleCardBox>
      )
    })
  }

  const onRenderBodyList = () => {
    return tokenList?.map((token, i) => {
      return (
        <StyleTableRow key={token.transactionHash}>
          <td>
            <StyleRowItemBox>
              <StyleNFTImageInList />
              <StyleTokenIdText>Vgames {token.token_id}</StyleTokenIdText>
            </StyleRowItemBox>
          </td>
          <td style={{ minWidth: '50px' }}>
            <p
              style={{
                width: 'fit-content',
                margin: 'auto',
              }}
            >
              0.1025 ETH
            </p>
          </td>
          <td style={{ minWidth: '50px' }}>
            <p style={{ width: 'fit-content', margin: 'auto' }}>0.0895 WETH</p>
          </td>
          <td style={{ minWidth: '50px' }}>
            <p style={{ width: 'fit-content', margin: 'auto' }}>0.09 WETH</p>
          </td>
          <td style={{ minWidth: '50px' }}>
            <p style={{ width: 'fit-content', margin: 'auto' }}>{167 + i}</p>
          </td>
          <td style={{ minWidth: '50px' }}>
            <p style={{ width: 'fit-content', margin: 'auto' }}>E63B14</p>
          </td>
        </StyleTableRow>
      )
    })
  }

  if (isLoading) {
    return (
      <StylePopWrapper>
        <StylePopUp>
          <StyleConnectWalletWrapper>
            <h2 style={{ color: '#fff', marginBottom: '20px' }}>Fetching!</h2>
          </StyleConnectWalletWrapper>
          <Loading width="80px" height="80px" />
        </StylePopUp>
      </StylePopWrapper>
    )
  }

  return (
    <StyledPage>
      <StyleTitle>My Inventory</StyleTitle>

      <StyleDisplayTypeWrapper>
        <StyleGridType
          size={30}
          style={{ background: displayType === 'grid' ? '#cccccc80' : '' }}
          onClick={() => {
            setDisplayType('grid')
            localStorage.setItem('DISPLAY-TYPE', 'grid')
          }}
        />
        <StyleListType
          size={30}
          style={{ background: displayType === 'list' ? '#cccccc80' : '' }}
          onClick={() => {
            setDisplayType('list')
            localStorage.setItem('DISPLAY-TYPE', 'list')
          }}
        />
      </StyleDisplayTypeWrapper>

      {account.address ? (
        displayType === 'grid' ? (
          <>
            <StyleTitle>Grid</StyleTitle>
            {tokenList && tokenList?.length > 0 ? <StyleGrid>{onRenderNftGrid()}</StyleGrid> : <p>No NFT display</p>}
          </>
        ) : (
          <>
            <StyleTitle>List</StyleTitle>
            <StyleTableWrapper>
              {tokenList && tokenList?.length > 0 ? (
                <StyleTable>
                  <thead style={{ borderRadius: '50px' }}>
                    <tr>
                      <StyleTableFirstTh>Items</StyleTableFirstTh>
                      <StyleTableTh>Current Price</StyleTableTh>
                      <StyleTableTh>Best Offer</StyleTableTh>
                      <StyleTableTh>Last Sale</StyleTableTh>
                      <StyleTableTh>Rarity</StyleTableTh>
                      <StyleTableLastTh>Owner</StyleTableLastTh>
                    </tr>
                  </thead>
                  <StyleTableBody>{onRenderBodyList()}</StyleTableBody>
                </StyleTable>
              ) : (
                <p>No NFT display</p>
              )}
            </StyleTableWrapper>
          </>
        )
      ) : (
        <StylePopWrapper>
          <StylePopUp>
            <StyleConnectWalletWrapper>
              <h2 style={{ color: '#fff' }}>Please connect your wallet to see your inventory!</h2>
            </StyleConnectWalletWrapper>
          </StylePopUp>
        </StylePopWrapper>
      )}
    </StyledPage>
  )
}

export default MyInventory
