import { CloseIcon, Container, useToast } from '@pancakeswap/uikit'
import ConnectWalletButton from 'components/ConnectWalletButton'
import { FormEvent, useState } from 'react'
import { FaAngleDoubleRight } from 'react-icons/fa'
import styled, { css, keyframes } from 'styled-components'
import { useAccount } from 'wagmi'
import { Web3 } from 'web3'

const StyledPage = styled.div`
  min-height: calc(100vh - 64px);
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #313131;
  position:relative ${({ theme }) => theme.mediaQueries.sm} {
    padding-top: 24px;
    padding-bottom: 24px;
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    padding-top: 32px;
    padding-bottom: 32px;
  }
`

const StyledWrapper = styled(Container)`
  height: 40vh;
  aspect-ratio: 1/1;
`
const imageAnimation = keyframes`
  0%, 100% {
    transform: rotate(3deg) translateY(2%);
  }
  50% {
    transform: rotate(-3deg) translateY(1%);
  }
`

const StyleNFTImage = styled.div`
  background: url('https://storage.googleapis.com/cdn.heroestd.io/Images/Box_Rare.png') center center / contain
    no-repeat;
  width: 100%;
  aspect-ratio: 1/1;
  margin-bottom: 40px;
  animation: ${css`
    ${imageAnimation} 3s linear 0s infinite normal none running
  `};
`

const StyleMintNFTButton = styled.button`
  border-radius: 10px;
  background: #21df02;
  width: 100%;
  height: auto;
  padding: 15px 0;
  font-size: 22px;
  font-weight: 600;
  color: black;
  border: none;
  cursor: pointer;
  transition: 0.5s all;
  margin: auto;

  &:hover {
    opacity: 0.7;
  }
`

const StylePopWrapper = styled.div`
  background: #00000050;
  padding: 20px;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const StylePopUp = styled.div`
  background: #27262c;
  padding: 20px;
  position: absolute;
`

const StyleQuantityForm = styled.form`
  position: relative;
`

const StyleCLoseIconWrapper = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  cursor: pointer;
`

const StyleQuantityInput = styled.input`
  background: #fff;
  color: gray;
  margin-top: 20px;
  width: 30vw;
`

const StyleSubmitQuantityButton = styled.button`
  border-radius: 10px;
  background: #21df02;
  height: auto;
  padding: 8px 10px;
  font-size: 18px;
  font-weight: 600;
  color: black;
  border: none;
  cursor: pointer;
  transition: 0.5s all;
  margin: 10px auto 0 auto;
  display: block;

  &:hover {
    opacity: 0.7;
  }
`

const StyleMyInventoryButton = styled.a`
  border-radius: 10px;
  background: white;
  height: auto;
  padding: 8px 10px;
  font-size: 18px;
  font-weight: 600;
  color: black;
  border: none;
  cursor: pointer;
  transition: 0.5s all;
  margin: 10px auto 0 auto;
  display: flex;
  justify-content: center;

  &:hover {
    opacity: 0.7;
  }
`

const StyleConnectWalletWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  align-items: center;
`

function MinNFT() {
  const [isOpenPopUp, setIsOpenPopUp] = useState(false)
  const account = useAccount()
  const { toastSuccess } = useToast()

  const handleOnOpenPopUp = () => {
    setIsOpenPopUp(true)
  }

  const handleOnClosePopUp = () => {
    setIsOpenPopUp(false)
  }

  const handleOnSubmit = async (e: FormEvent) => {
    e.preventDefault()
    const web3 = new Web3('https://data-seed-prebsc-2-s2.bnbchain.org:8545')
    const _owner = account.address
    const _quantity = e.target[0].value

    const encodedFunction = web3.eth.abi.encodeFunctionCall(
      {
        inputs: [
          {
            internalType: 'address',
            name: '_owner',
            type: 'address',
          },
          {
            internalType: 'uint256',
            name: '_quantity',
            type: 'uint256',
          },
          {
            internalType: 'bytes32[]',
            name: '_merkleProof',
            type: 'bytes32[]',
          },
        ],
        name: 'mint',
        outputs: [],
        stateMutability: 'payable',
        type: 'function',
      },
      [_owner, _quantity, []],
    )

    const transactionParameters = {
      to: '0xF2e5482c230cb62b1363039E4128a8f2A7Ce8f5D' as `0x${string}`,
      from: _owner as `0x${string}`,
      data: encodedFunction as `0x${string}`,
    }

    await window.ethereum
      ?.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
      })
      .then((res) => {
        if (res) {
          toastSuccess('Mint successfully', 'Go to My Inventory to see more!')
        }
      })
      .catch((err) => {
        toastSuccess('Mint failed')
      })
    handleOnClosePopUp()
  }

  return (
    <StyledPage>
      <StyledWrapper>
        <StyleNFTImage />
        <StyleMintNFTButton onClick={handleOnOpenPopUp}>MINT NFT</StyleMintNFTButton>
        <StyleMyInventoryButton href="/my-inventory">
          My Inventory <FaAngleDoubleRight />
        </StyleMyInventoryButton>
      </StyledWrapper>
      {isOpenPopUp ? (
        <StylePopWrapper>
          <StylePopUp>
            {account.address ? (
              <StyleQuantityForm action="" onSubmit={handleOnSubmit}>
                <h2 style={{ color: '#fff' }}>Quantity Number</h2>
                <StyleQuantityInput autoFocus type="number" />
                <StyleSubmitQuantityButton type="submit">Submit</StyleSubmitQuantityButton>
              </StyleQuantityForm>
            ) : (
              <StyleConnectWalletWrapper>
                <h2 style={{ color: '#fff' }}>Please connect your wallet before minting!</h2>
                <ConnectWalletButton />
              </StyleConnectWalletWrapper>
            )}
            <StyleCLoseIconWrapper onClick={handleOnClosePopUp}>
              <CloseIcon color="#fff" />
            </StyleCLoseIconWrapper>
          </StylePopUp>
        </StylePopWrapper>
      ) : (
        <></>
      )}
    </StyledPage>
  )
}

export default MinNFT
